$(document).ready(function(){

	//FORMULARIO CATEGORIAS - TIPOS

	$('#add-categoria-modal').on('shown.bs.modal', function () {
		form = "Tipo: ";
		modal = $(this);
		addForm();
	});

	$('#add-marca-modal').on('shown.bs.modal', function () {
		form = "Modelo: ";
		modal = $(this);
		addForm();
	});

	$('#edit-categoria-modal').on('shown.bs.modal', function () {
		form = "Tipo: ";
		modal = $(this);
		editForm();
	});
	
	$('#edit-marca-modal').on('shown.bs.modal', function () {
		form = "Modelo: ";
		modal = $(this);
		editForm();
	});

	$(document).on('hide.bs.modal','#add-categoria-modal', function () {
		modal = $(this);
		$('#form-categoria-add')[0].reset();
		modal.find('#add-general').empty();
		$('.btn-remove').remove();
		$('#preview-image-add').attr('src', null);
		$('#preview-image-add').addClass('transparent');
		$('#span-add').html(null);
		quitarMensajes();
	});

	$(document).on('hide.bs.modal','#edit-categoria-modal', function () {
		$('#form-categoria-edit')[0].reset();
		modal.find('#edit-general').empty();
		$('.btn-remove').remove();
		$('#hidden-delete').remove();
		$('#preview-image-edit').attr('src', null);
		$('#preview-image-edit').addClass('transparent');
		$('#span-edit').html(null);
		quitarMensajes();
	});

	$(document).on('hide.bs.modal','#add-marca-modal', function () {
		$('#form-marca-add')[0].reset();
		modal.find('#add-general').empty();
		$('.btn-remove').remove();
		quitarMensajes();
	});

	$(document).on('hide.bs.modal','#edit-marca-modal', function () {
		$('#form-marca-edit')[0].reset();
		modal.find('#edit-general').empty();
		$('.btn-remove').remove();
		$('#hidden-delete').remove();
		quitarMensajes();
	});

	$('.add-dyn-input').on('click', function() {
		i = $('.input-text').size();
		if($(this).attr('id') == 'add'){
			if(i == 1){
				modal.find('#buttons-add').append('<button type="button" class="btn btn-danger btn-remove"><i class="fa fa-trash"></i>Quitar</button>');
			}
			modal.find('#add-general').append('<p id="input-'+ i +'">\
				<input type="text" class="form-control boxed input-text" id="dyn-input-'+ i +'" size="20" name="dynamicInput[]" value="" placeholder="Ingrese '+ form + (i + 1) +'" />\
			</p>');
			i++;
		}else{
			if(i == 1){
				modal.find('#buttons-edit').append('<button type="button" class="btn btn-danger btn-remove"><i class="fa fa-trash"></i>Quitar</button>');
			}
				modal.find('#edit-general').append('<p id="input-'+ i +'">\
					<input type="text" class="form-control boxed input-text" id="dyn-input-'+ i +'" size="20" name="dynamicInput[]" value="" placeholder="Ingrese '+ form + (i + 1) +'" />\
				</p>');
			i++;
		}
	});

	$(document).on('focus', '.input-text', function() {
		$('.input-text').removeClass('selected');
		idInput = $(this).attr('id').substring(10);
		valInput = $('#dyn-input-id-'+ idInput).attr('value');
		$(this).addClass('selected');
	});
	
	$(document).on('click', '.btn-remove', function(){
		if(idInput){
			if(valInput != undefined){
				modal.find('#edit-general').append('<input type="hidden" name="delete[]" value="'+ valInput +'">');
			}

			i = $('.input-text').size();
			if(i == 2){
				$('.btn-remove').remove();
			}
			$('#input-'+ idInput).remove();
			for(var j = parseInt(idInput) + 1; j < i; j++){
				$('#input-'+ j).attr('id', "input-" + (j - 1));
				$('#dyn-input-'+ j).attr('placeholder', "Ingrese "+ form + j);
				$('#dyn-input-'+ j).attr('id', "dyn-input-" + (j - 1));
				$('#dyn-input-id-'+ j).attr('id', "dyn-input-id-" + (j - 1));
			}
			idInput = false;
		}
	});

	//FORMULARIO AGREGAR CATEGORIA
	$('#form-categoria-add').on('submit', function(event){
		event.preventDefault();
		quitarMensajes();
		var formCategoriaAdd = document.getElementById('form-categoria-add');
    	var formData = new FormData(formCategoriaAdd);
		$.ajax({
			url: route.set_categoria,
			method: "POST",
			data: formData,
			dataType: "json",
			cache: false,
			contentType: false,
			processData: false,
			success:function(data){
					$('#add-categoria-modal').modal('hide');
					$(data).insertBefore('#output-success-categoria').delay(3000).fadeOut();
					$('#form-categoria-add')[0].reset();
					$('#categorias-table').DataTable().ajax.reload();
			},
			error: function(data)
            {
            	for(error in data.responseJSON.errors){
					if (error == "categoria") {
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-1">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-categoria');
					}else{						
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-2">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-tipo');
						return false;
					}
                }
			}
		})
	});

	//FORMULARIO EDITAR CATEGORIA
	$('#form-categoria-edit').on('submit', function(event){
		event.preventDefault();
		quitarMensajes();
		$('#edit-general.tipo').append('<input id="hidden-delete" type="hidden" name="delete[]" value="">');
		var formCategoriaEdit = document.getElementById('form-categoria-edit');
		var formData = new FormData(formCategoriaEdit);
		$.ajax({
			url: route.upd_categoria,
			method: "POST",
			data: formData,
			dataType: "json",
			cache: false,
			contentType: false,
			processData: false,
			success:function(data){
					$('#edit-categoria-modal').modal('hide');
					$(data.success).insertBefore('#output-success-categoria').delay(3000).fadeOut();
					$('#form-categoria-edit')[0].reset();
					$('#categorias-table').DataTable().ajax.reload();
			},
			error: function(data){
            	for(error in data.responseJSON.errors){
					if (error == "categoria") {
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-1">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-categoria-edit');
					}else{						
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-2">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-tipo-edit');
						return false;
					}
                }
			}
		})
	});

	//FORMULARIO ELIMINAR CATEGORIA
	$('#form-categoria-del').on('submit', function(event){
		event.preventDefault();
		form_categoria_del = $(this).serialize();
		$.ajax({
			url: route.del_categoria,
			method: "DELETE",
			data: form_categoria_del,
			dataType: "json",
			success:function(data){
				$('#delete-categoria-modal').modal('hide');
				$(data).insertBefore('#output-success-categoria').delay(3000).fadeOut();
				$('#categorias-table').DataTable().ajax.reload();
			}
		})
	});

	//FORMULARIO AGREGAR MARCA
	$('#form-marca-add').on('submit', function(event){
		event.preventDefault();
		quitarMensajes();
		formData = $(this).serialize();
		$.ajax({
			url: route.set_marca,
			method: "POST",
			data: formData,
			dataType: "json",
			success:function(data){
					$('#add-marca-modal').modal('hide');
					$(data).insertBefore('#output-success-marca').delay(3000).fadeOut();
					$('#form-marca-add')[0].reset();
					$('#marcas-table').DataTable().ajax.reload();
			},
			error: function(data)
            {
            	for(error in data.responseJSON.errors){
					if (error == "marca") {
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-1">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-marca');
					}else{						
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-2">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-modelo');
						return false;
					}
                }
			}
		})
	});

	//FORMULARIO EDITAR MARCA
	$('#form-marca-edit').on('submit', function(event){
		event.preventDefault();
		quitarMensajes();
		$('#edit-general.modelo').append('<input id="hidden-delete" type="hidden" name="delete[]" value="">');
		form_marca_edit = $(this).serialize();
		$.ajax({
			url: route.upd_marca,
			method: "PATCH",
			data: form_marca_edit,
			dataType: "json",
			success:function(data){
					$('#edit-marca-modal').modal('hide');
					$(data.success).insertBefore('#output-success-marca').delay(3000).fadeOut();
					$('#form-marca-edit')[0].reset();
					$('#marcas-table').DataTable().ajax.reload();
			},
			error: function(data){
            	for(error in data.responseJSON.errors){
					if (error == "marca") {
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-1">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-marca-edit');
					}else{						
						$('<div class="alert alert-danger alert-dismissible fade in" id="alert-error-2">\
							<button class="close" type="button" data-dismiss="alert">&times</button>'
							+ data.responseJSON.errors[error] + '</div>').insertBefore('#div-modelo-edit');
						return false;
					}
                }
			}
		})
	});

	//FORMULARIO ELIMINAR MARCA
	$('#form-marca-del').on('submit', function(event){
		event.preventDefault();
		form_marca_del = $(this).serialize();
		$.ajax({
			url: route.del_marca,
			method: "DELETE",
			data: form_marca_del,
			dataType: "json",
			success:function(data){
				$('#delete-marca-modal').modal('hide');
				$(data).insertBefore('#output-success-marca').delay(3000).fadeOut();
				$('#marcas-table').DataTable().ajax.reload();
			}
		})
	});

	//FORMULARIO ELIMINAR PRODUCTO
	$('#form-producto-del').on('submit', function(event){
		event.preventDefault();
		form_producto_del = $(this).serialize();
		$.ajax({
			url: route.del_producto,
			method: "DELETE",
			data: form_producto_del,
			dataType: "json",
			success:function(data){
				$('#delete-producto-modal').modal('hide');
				$(data).insertBefore('#output-success-producto').delay(3000).fadeOut();
				$('#productos-table').DataTable().ajax.reload();
			}
		})
	});

	var addForm = function(){
		idInput = false;
		modal.find('#add-general').append('<p id="input-0">\
			<input type="text" class="form-control boxed input-text" id="dyn-input-0" size="20" name="dynamicInput[]" value="" placeholder="Ingrese '+ form +'1" />\
		</p>');
	}

	var editForm = function(){
		idInput = false;
		if($('.input-text').size() > 1){
			modal.find('#buttons-edit').append('<button type="button" class="btn btn-danger btn-remove"><i class="fa fa-trash"></i>Quitar</button>');
		}
	}

	var quitarMensajes = function(){
		$('#alert-error-1').remove();
		$('#alert-error-2').remove();
	}

});