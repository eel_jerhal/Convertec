$(document).ready(function () {
    var lang = {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": '<button class="btn btn-success btn-tabla"><i class="fas fa-angle-double-left"></i></button>',
            "sLast": '<button class="btn btn-success btn-tabla"><i class="fas fa-angle-double-right"></i></button>',
            "sNext": '<button class="btn btn-success btn-tabla"><i class="fas fa-angle-right"></i></button>',
            "sPrevious": '<button class="btn btn-success btn-tabla"><i class="fas fa-angle-left"></i></button>'
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    };

    var renderHtmlProd = function (modal, data) {
        html = '<div class= "row">\
            <div class="center-block">\
                <button type="button" id="eliminar" class="table-btn-eliminar btn btn-danger btn-tabla center-block" data-toggle="modal" data-target="#delete-' + modal + '-modal" title="Eliminar Item Seleccionado">\
                    <i class="fa fa-trash"></i>\
                </button>\
            </div>\
            <div class="center-block">\
                <a href="/admin/productos/' + data + '/edit" class="table-btn-editar btn btn-success btn-tabla center-block" style="margin-bottom: 0px;" title="Editar Item Seleccionado">\
                    <i class="fa fa-pencil"></i>\
                </a>\
            </div>\
        </div>'
        return html;
    }

    var renderHtml = function (modal) {
        html = '<div class= "row">\
            <div class="center-block">\
                <button type="button" id="table-btn-eliminar" class="table-btn-eliminar btn btn-danger btn-tabla center-block" data-toggle="modal" data-target="#delete-' + modal + '-modal" title="Eliminar Item Seleccionado">\
                    <i class="fa fa-trash"></i>\
                </button>\
            </div>\
            <div class="center-block">\
                <button type="button" id="table-btn-editar" class="table-btn-editar btn btn-success btn-tabla center-block" data-toggle="modal" data-target=#edit-' + modal + '-modal style="margin-bottom: 0px;" title="Editar Item Seleccionado">\
                    <i class="fa fa-pencil"></i>\
                </button>\
            </div>\
        </div>'
        return html;
    }

    //Tabla productos
    var prod_table = $('#productos-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        ajax: {
            url: route.get_productos, //variable que guarda la ruta --- referencia en admin.blade
            datatype: "json"
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',
                "render": function () {
                    return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                },
                width:"15px"
            },
            {
                data: 'imagen',
                name: 'imagen',
                render: function (data) {
                    return '<img class="img-table" src="' + data + '">';
                }
            },
            {
                data: 'codigo',
                name: 'codigo'
            },
            {
                data: 'nombre',
                name: 'nombre'
            },
            {
                data: 'categoria',
                name: 'categoria'
            },
            {
                data: 'tipo',
                name: 'tipo'
            },
            {
                data: 'marca',
                name: 'marca'
            },
            {
                data: 'modelo',
                name: 'modelo'
            },
            {
                data: 'id',
                name: 'id',
                render: function (data) {
                    return renderHtmlProd('producto', data);
                }
            }
        ],
        columnDefs: [
            {
                targets: 0,
                orderable: false,
                searchable: false,
                width: "8%"
            },
            {
                targets: 1,
                orderable: false,
                searchable: false,
                width: "8%"
            },
            {
                targets: 8,
                orderable: false,
                searchable: false,
                width: "25%"
            }
        ],
        order: [
            [2, "asc"]
        ]
    });
    
    //Tabla categorias
    var cat_table = $('#categorias-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        ajax: {
            url: route.get_categorias,
            datatype: "json"
        },
        columns: [{
                data: 'imagen',
                name: 'imagen',
                render: function (data) {
                    return '<img class="img-table" src="' + data + '">';
                }
            },
            {
                data: 'categoria',
                name: 'categoria'
            },
            {
                data: 'tipos',
                name: 'tipos',
                render: function (data){
                    var tipos = "<ul>";
                    $.each($(data),function(index, value){
                        tipos += "<li>" + value.tipo + "</li>";
                    });
                    tipos += "</ul>"
                    return tipos;
                }
            },
            {
                render: function () {
                    return renderHtml('categoria');
                }
            }
        ],
        columnDefs: [{
                targets: 0,
                orderable: false,
                searchable: false,
                width: "10%"
            },
            {
                targets: 2,
                orderable: false,
                searchable: true
            },
            {
                targets: 3,
                orderable: false,
                searchable: false,
                width: "22%"
            }
        ]
    });

    //tabla marcas
    var marcas_table = $('#marcas-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        ajax: {
            url: route.get_marcas,
            datatype: "json"
        },
        columns: [{
                data: 'marca',
                name: 'marca'
            },
            {
                data: 'modelos',
                name: 'modelos',
                render: function (data){
                    var modelos = "<ul>";
                    $.each($(data),function(index, value){
                        modelos += "<li>" + value.modelo + "</li>";
                    });
                    modelos += "</ul>"
                    return modelos;
                }
            },
            {
                render: function () {
                    return renderHtml('marca');
                }
            }
        ],
        columnDefs: [{
                targets: 1,
                orderable: false,
                searchable: true
            },
            {
                targets: 2,
                orderable: false,
                searchable: false,
                width: "22%"
            }
        ]
    });


    var tbody = ['#productos-table tbody', '#categorias-table tbody', '#marcas-table tbody'];
    var table = [prod_table, cat_table, marcas_table]

    // evento al abrir y cerrar detalles del producto
    $(tbody[0]).on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var tdi = tr.find("i.fa");
        var row = table[0].row(tr);

        if (row.child.isShown()) {
            //Si está abierto se cierra
            row.child.hide();
            tr.removeClass('shown');
            tdi.first().removeClass('fa-minus-square');
            tdi.first().addClass('fa-plus-square');
        }
        else {
            // Abre la pestaña
            row.child(format(row.data())).show();
            tr.addClass('shown');
            tdi.first().removeClass('fa-plus-square');
            tdi.first().addClass('fa-minus-square');
        }
    });

    table[0].on("user-select", function (e, dt, type, cell, originalEvent) {
        if ($(cell.node()).hasClass("details-control")) {
            e.preventDefault();
        }
    });

    function format(d){
        
        // `d` data original de la fila
        //Si existe pdf se genera botón
        var btnDescarga;
        if(d.ficha){
            btnDescarga = 
            '<div class="row">' +
                '<div class="col-md-4 center-block">' +
                    '<a class="btn btn-success btn-tabla" href="storage/' + d.ficha + '" download="'+ d.ficha.slice(4)+'" title="Descargar Ficha">' +
                        '<i class="fa fa-download"></i> Descargar' +
                    '</a>' +
                '</div>' +
                '<div class="col-md-4 center-block">' +
                    '<a href="storage/' + d.ficha + '" target="_blank" class="btn btn-success btn-tabla" title="Ver Ficha">' +
                        '<i class="fa fa-eye"></i> Ver ficha ' +
                    '</a>' +
                '</div>' + 
            '</div>';
        }else{
            btnDescarga = "No posee";
        }

        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
                '<td>Ficha Técnica:</td>' +
                '<td>' + btnDescarga + '</td>' +
            '</tr>' +
            '<tr>' +
                '<td>Descripción:</td>' +
                '<td>' + $("<td>").html(d.descripcion).text() + '</td>' +
            '</tr>' +
        '</table>';
   }

    //TBODY[0] Y TABLE[0] SON REFERENCIA A PRODUCTOS
    $(tbody[0]).on("click", "button.table-btn-eliminar", function () {
        var data = table[0].row($(this).parents("tr")).data();
        $('#delete-producto-modal').on("show.bs.modal", function () {
            var modal = $(this);
            document.getElementById('producto-del').innerHTML = 'Estas seguro que quieres eliminar el producto "' + data.codigo + '"?';
            modal.find('.modal-body #prod-id').val(data.id);
        });
    });


    //TBODY[1] Y TABLE[1] SON REFERENCIA A CATEGORIAS
    $(tbody[1]).on("click", "button.table-btn-editar", function () {

        var data = table[1].row($(this).parents("tr")).data();
        $('#categoria-edit').val(data.categoria);
        $('#categoria-id-edit').val(data.id);
        $('#preview-image-edit').attr('src', data.imagen);
        var iName = data.imagen;
        iName = iName.substring(
            iName.lastIndexOf("/") + 1,
            iName.lastIndexOf(".")) //Image Name
            .concat(iName.substring(iName.lastIndexOf(".")) //Extension
        );
        $('#span-edit').html(iName);

        for (var i in data.tipos) {
            $('<p id="input-' + i + '">\
        <input type="text" class="form-control boxed input-text" id="dyn-input-' + i + '" size="20" name="dynamicInput[]" value="" placeholder="Ingrese Tipo: ' + i + '" />\
        <input type="hidden" name="dynamicInput_id[]" id="dyn-input-id-' + i + '" value="">\
    </p>').appendTo($('#edit-general.tipo'));
            $('#dyn-input-' + i + '').val(data.tipos[i].tipo);
            $('#dyn-input-id-' + i + '').val(data.tipos[i].id);
        }
    });

    $(tbody[1]).on("click", "button.table-btn-eliminar", function () {
        var data = table[1].row($(this).parents("tr")).data();
        $('#delete-categoria-modal').on("show.bs.modal", function () {
            var modal = $(this);
            document.getElementById('categoria-del').innerHTML = 'Al borrar la Categoría "' + data.categoria + '". Se eliminarán los Tipos asociados a esa Categoría.\n Estas seguro que quieres continuar?';
            modal.find('.modal-body #cat-id').val(data.id);
        });
    });

    //TBODY[2] Y TABLE[2] SON REFERENCIA A MARCAS
    $(tbody[2]).on("click", "button.table-btn-editar", function () {
        var data = table[2].row($(this).parents("tr")).data();
        $('#marca-edit').val(data.marca);
        $('#marca-id-edit').val(data.id);

        for (var i in data.modelos) {
            $('<p id="input-' + i + '">\
        <input type="text" class="form-control boxed input-text" id="dyn-input-' + i + '" size="20" name="dynamicInput[]" value="" placeholder="Ingrese Modelo: ' + i + '" />\
        <input type="hidden" name="dynamicInput_id[]" id="dyn-input-id-' + i + '" value="">\
    </p>').appendTo($('#edit-general.modelo'));
            $('#dyn-input-' + i + '').val(data.modelos[i].modelo);
            $('#dyn-input-id-' + i + '').val(data.modelos[i].id);
        }
    });

    $(tbody[2]).on("click", "button.table-btn-eliminar", function () {
        var data = table[2].row($(this).parents("tr")).data();
        $('#delete-marca-modal').on("show.bs.modal", function () {
            var modal = $(this);
            document.getElementById('marca-del').innerHTML = 'Al borrar la Marca "' + data.marca + '". Se eliminarán los Modelos asociados a la Marca seleccionada.\n Estas seguro que quieres continuar?';
            modal.find('.modal-body #marca-id').val(data.id);
        });
    });

});