/*Editor de texto*/

var editor_config = {
    path_absolute : "{{ URL::to('/') }}/",
    selector: "textarea#descripcion",
    language: 'es_MX',
    statusbar: false,
    menubar: false,
    plugins: [
        "advlist autolink lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "paste textcolor colorpicker textpattern"
    ],
    toolbar: "newdocument print | undo redo | bold italic underline removeformat | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table hr | preview fullscreen",
    relative_urls: false,
    height: 250,
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
};
tinymce.init(editor_config);