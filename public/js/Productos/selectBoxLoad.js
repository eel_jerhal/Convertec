$(document).ready(function() {
    $(function(){
        var edit = $('#edit-producto').length; 
        if(edit){
            cargarTipos(valorSeleccionado.categoria, edit);
            cargarModelos(valorSeleccionado.marca, edit);

            $('select[name="categoria"]').val(valorSeleccionado.categoria);	//select categoria
            $('select[name="marca"]').val(valorSeleccionado.marca);			//select marca
            $('#preview-image').attr('src', valorSeleccionado.image);	//carga la imagen
            edit = null;
        }

        if (error.error){
            $("select[name=categoria] option[value="+ error.categoria +"]").prop('selected', true);
            cargarTipos(error.categoria, null);
            $("select[name=marca] option[value="+ error.marca +"]").prop('selected', true);
            cargarModelos(error.marca, null);
            error.error = null;
        }
    });

    $('select[name="categoria"]').on('change', function(){
        categoriaId = $(this).val();
        cargarTipos(categoriaId, null);
    });

    $('select[name="marca"]').on('change', function(){
        marcaId = $(this).val();
        cargarModelos(marcaId, null);
    });

    function cargarTipos(categoriaId, edit){
        if(categoriaId) {
            $.ajax({
                url: '/admin/productos/get_tipos/' + categoriaId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader-tipos').toggleClass('d-none');
                },
                success:function(data) {
                    $('select[name="tipo"]').empty();
                    $.each(data, function(key, value){
                        $('select[name="tipo"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
                    $('select[name="tipo"]').prop('disabled', false);
                },
                complete: function(){
                    $('#loader-tipos').toggleClass('d-none');
                    if (error.tipo) {
                        $("select[name=tipo] option[value="+ error.tipo +"]").prop('selected', true);
                        error.tipo = null;
                    }

                    if(edit){
                        $('select[name="tipo"]').val(valorSeleccionado.tipo);
                    }
                }
            });
        } else {
            $('select[name="tipo"]').empty();
            $('select[name="tipo"]').append('<option value="">--Seleccione Tipo--</option>');
            $('select[name="tipo"]').prop('disabled', 'disabled');
        }
    }

    function cargarModelos(marcaId, edit){
        if(marcaId) {
            $.ajax({
                url: '/admin/productos/get_modelos/' + marcaId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader-modelos').toggleClass('d-none');
                },
                success:function(data) {
                    $('select[name="modelo"]').empty();
                    $.each(data, function(key, value){
                        $('select[name="modelo"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
                    $('select[name="modelo"]').prop('disabled', false);
                },
                complete: function(){
                    $('#loader-modelos').toggleClass('d-none');
                    if (error.modelo) {
                        $("select[name=modelo] option[value="+ error.modelo +"]").prop('selected', true);
                        error.modelo = null;
                    }

                    if(edit){
                        $('select[name="modelo"]').val(valorSeleccionado.modelo);
                    }
                }
            });
        } else {
            $('select[name="modelo"]').empty();
            $('select[name="modelo"]').append('<option value="">--Seleccione Modelo--</option>');
            $('select[name="modelo"]').prop('disabled', 'disabled');
        }
    }
});