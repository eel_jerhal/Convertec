/*
	Image upload
*/

'use strict';

(function ($, window, document, undefined) {
	$('.inputfile').each(function () {
		var $input = $(this),
			$label = $input.next('label'),
			labelVal = $label.html();

		$input.on('change', function (e) {
			var fileName = '';

			if (this.files && this.files.length > 1)
				fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
			else if (e.target.value)
				fileName = e.target.value.split('\\').pop();

			if (fileName)
				$label.find('span').html(fileName);
			else
				$label.html(labelVal);
		});

		// Firefox bug fix
		$input
			.on('focus', function () { $input.addClass('has-focus'); })
			.on('blur', function () { $input.removeClass('has-focus'); });
	});
})(jQuery, window, document);


function readURL(input, type) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		if (type.search('image') != -1) {
			reader.onload = function (e) {
				$('#preview-' + type)
					.attr('src', e.target.result)
					.removeClass('transparent')
			};
			reader.readAsDataURL(input.files[0]);
		} else {
			reader.onload = function (e) {
				$('#preview-' + type)
					.attr('href', e.target.result)
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
}