@extends('layouts.front')

@section('styles')
    <link href="{{asset('css/shop-item.css')}}" rel="stylesheet">
    <link href="{{asset('css/productos.css')}}" rel="stylesheet">
@endsection

@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <div class="container">
            <a href="{{ route('welcome')}}" class="navbar-brand">
                <img class="logo" src="{{asset('img/logos/logo_completo.png')}}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home
                        <span class="sr-only">(current)</span>
                        </a>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 sidebar">
                <h1 class="my-4">Categorías</h1>
                <div class="list-group">
                    @foreach ($categorias as $categoria)
                        <a href="#{{$categoria->categoria}}" class="list-group-item categoria" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">
                            {{$categoria->categoria}}
                            <div class="fa-pull-right">
                                <span class="fa fa-caret-square-down"></span>
                            </div>
                        </a>
                        <div class="collapse" id="{{$categoria->categoria}}">
                            <div class="list-group">
                                @foreach ($categoria->tipos as $tipo)
                                    <a href="#" class="list-group-item child">
                                        <hr>
                                        {{$tipo->tipo}}
                                        <hr>
                                        {{-- <div class="fa-pull-left">
                                            <span class="fa fa-caret-right fa-xs"></span>
                                        </div> --}}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-9">
                <div class="container">
                    <div class="row">
                        @foreach ($productos as $producto)
                            <div class="col-lg-4 col-md-6 mb-4">
                                <div class="card h-100">
                                    <a href="#">
                                        <img class="card-img-top img-thumbnail" src="{{asset($producto->imagen)}}" alt="">
                                    </a>
                                    <div class="card-body">
                                        <h4 class="card-title text-center">
                                            <a href="#">{{$producto->nombre}}</a>
                                        </h4>
                                        <p class="card-text">{!! $producto->descripcion !!}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @unless (count($productos))
                            <div class="card">
                                <h1>Lo sentimos, En este momento no hay existencia de productos.</h1>
                                <p>Pongase en <a href="#">contacto</a> con nosotros para mas información</p>
                            </div>
                        @endunless
                    </div>
                </div>
                {{ $productos->links() }}
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <footer class="py-5 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ml-auto text-center">
                    <p class="m-0 text-center text-white">Copyright © 2018 & Convertec, Derechos Reservados</p>
                </div>
            </div>
        </div>
    </footer>
@endsection

@section('scripts')
    <script>
        $('.categoria').on('click', function() {
            $(this).find('span').toggleClass('fa-caret-square-up');
        });
    </script>
@endsection