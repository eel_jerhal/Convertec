@extends('layouts.front')

@section('styles')
    <!-- Custom styles for this template -->
    <link href="{{asset('css/modern-business.css')}}" rel="stylesheet">
    <link href="{{asset('css/creative.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <!-- CSS de slick -->
    <link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}"/>
@endsection

@section('nav')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a href="#inicio" class="navbar-brand js-scroll-trigger">
                <img class="logo" src="{{asset('img/logos/logo_completo.png')}}">
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#inicio">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#empresa">Empresa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#servicios">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#productos">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#clientes">Clientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contacto">Contacto</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@endsection

@section('header')
    <header id="inicio" class="masthead">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item masthead active" style="background-image: url('img/header/headerct.jpg')">
                </div>
                <div class="carousel-item masthead" style="background-image: url('img/header/headerct2.jpg')">
                </div>
                <div class="carousel-item masthead" style="background-image: url('img/header/headerct3.jpg')">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="fa fa-2x fa-chevron-circle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="fa fa-2x fa-chevron-circle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
    </header>
@endsection

@section('content')
    <section id="empresa" class="bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <h2 class="section-heading text-d-green">Nuestra Empresa</h2>
                    <hr class="light my-4">
                    <p class="text-d-green mb-4">A la fecha, la empresa cuenta con un sólido prestigio por la calidad de sus productos y la excelencia de su servicio, transformándose esa fortaleza en un norte que guía nuestro quehacer diario, siendo su principal preocupación la satisfacción de sus clientes.</p>
                    <hr class="light my-4">
                    <div class="container-fluid p-0">
                        <div class="row popup-gallery">
                            <div class="col-lg-6 col-sm-6">
                                <a href="img/certificaciones/cert_iso9001.jpg" title="Certificación - ISO 9001">
                                <img class="img-fluid cert" src="img/certificaciones/iso9001.png" alt="Certificación ISO 9001">
                                </a>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <a href="img/certificaciones/cert_ohsas18001.jpg" title="Certificación - OHSAS 18001">
                                <img class="img-fluid cert" src="img/certificaciones/ohsas18001.png" alt="Certificación OHSAS 18001">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="servicios">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Nuestros Servicios</h2>
                    <hr class="my-4">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-wrench text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Mantenimiento y Pruebas de Baterías y Cargadores</h3>
                        <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque varius, justo id volutpat placerat, neque.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-calendar-o text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Bancos de Baterías</h3>
                        <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque varius, justo id volutpat placerat, neque.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-flash text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Inversores de Tensión Sinusoidales</h3>
                        <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque varius, justo id volutpat placerat, neque.</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-plug text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Cargadores de Baterías</h3>
                        <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque varius, justo id volutpat placerat, neque.</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 col-md-12 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fa fa-4x fa-gear text-primary mb-3 sr-icons"></i>
                        <h3 class="mb-3">Instrumentos y Accesorios</h3>
                        <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque varius, justo id volutpat placerat, neque.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="productos" class="text-white bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Nuestros Productos</h2>
                    <hr class="my-4">
                </div>
            </div>
        </div>
        <div class="container-fluid p-0 parallax">
            <div class="row no-gutters">
                @foreach ($categorias as $categoria)
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ route('categoria', ['id' => "$categoria->id"]) }}" title="{{$categoria->categoria}}">
                        <img class="img-fluid" src="{{asset($categoria->imagen)}}" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-name">
                                        {{$categoria->categoria}}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="bg-dark text-white" id="clientes">
        <div class="container text-center">
            <h2 class="mb-4">Nuestros Clientes</h2>
            <hr class="my-4">
            <div class="clientes">
                <div><img src="img/clientes/aes.jpg"></div>
                <div><img src="img/clientes/anglo.jpg"></div>
                <div><img src="img/clientes/arauco.jpg"></div>
                <div><img src="img/clientes/bosch.jpg"></div>
                <div><img src="img/clientes/cap.jpg"></div>
                <div><img src="img/clientes/chilquinta.jpg"></div>
                <div><img src="img/clientes/codelco.jpg"></div>
                <div><img src="img/clientes/colbun.jpg"></div>
                <div><img src="img/clientes/endesa.jpg"></div>
                <div><img src="img/clientes/escondida.jpg"></div>
            </div>
        </div>
    </section>
    <section id="contacto" class="bottom-mod">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mx-auto text-center">
                    <h2 class="section-heading">Contáctenos</h2>
                    <hr class="my-4">
                    <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque varius, justo id volutpat placerat, neque.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 ml-auto text-center">
                    <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                    <p>(56) 22 775 6832</p>
                    <p>(56) 22 775 6827</p>
                </div>
                <div class="col-lg-4 mr-auto text-center">
                    <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                    <p>
                        <a href="mailto:convertec@convertec.cl">convertec@convertec.cl</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    <footer class="py-5 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ml-auto text-center">
                    <p class="m-0 text-center text-white">Copyright © 2018 & Convertec, Derechos Reservados</p>
                </div>
            </div>
        </div>
    </footer>
@endsection

@section('scripts')
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/scrollreveal/scrollreveal.js')}}"></script>
    <script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <!-- Custom scripts for this template -->
    <script src="{{asset('js/creative.js')}}"></script>
    <!-- scripts de slick -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script type="text/javascript" src="js/slick.clientes.js"></script>
@endsection