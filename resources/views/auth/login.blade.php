<!doctype html>
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ config('app.name') }} - Administración</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset('img/logos/favicon.ico')}}" />
        <!-- Place favicon.ico in the root directory -->
        <link href="{{ asset('css/vendor.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" id="theme-style">
        <link href="{{ asset('css/mod-admin.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="auth">
            <div class="auth-container">
                <div class="card">
                    <header class="auth-header">
                        <h1 class="auth-title">
                            <div class="logo"> <img class="logo" src="{{asset('img/logos/convertec_icon.png')}}"> </div>
                            {{ config('app.name') }} - Administración
                        </h1>
                    </header>
                    <div class="auth-content">
                        <form id="login-form" method="POST" action="{{ route('login') }}" novalidate="">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" name="email" placeholder="Email" required>
                                @if ($errors->has('email'))
                                    <span id="login-error" class="has-error">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif  
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" required>
                            </div>
                            <div class="form-group">
                                <label for="remember">
                                    <input class="checkbox" id="remember" type="checkbox"> 
                                    <span>Recuerdame</span>
                                </label>
                                <a href="#" class="forgot-btn pull-right">Olvidaste tu Contraseña?</a> 
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-block btn-primary border-round">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-xs-center">
                    <a href="{{ url('/') }}" class="btn btn-secondary rounded btn-sm">
                        <i class="fa fa-arrow-left"></i> Volver a Página Principal
                    </a> 
                </div>
            </div>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script>
            $('#email').focusout(function() {
                $('#login-error').remove();
            });
        </script>
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>