@section('modals')
    <!-- MODAL PRODUCTO -->
    <div class="modal fade" id="delete-producto-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-danger"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form id="form-producto-del">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="producto-del"></p>
                        <input type="hidden" name="producto_id" id="prod-id" value="">
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" class="btn btn-primary" id="action" value="Si"></input>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- FIN MODAL PRODUCTO -->
    <!-- MODAL CATEGORIAS -->
    <div class="modal fade" id="add-categoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class="fa fa-warning"></i>Nueva Categoría</h4>
                </div>
                <form id="form-categoria-add">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group" id="div-categoria">
                            <label>Categoría:</label>
                            <input type="text" class="form-control boxed" name="categoria" id="categoria-add" placeholder="Ingrese Categoría">
                        </div>
                        <div class="form-group" id="div-tipo">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tipos:</label>
                                </div>
                                <div class="col-md-6" id="buttons-add">
                                    <button type="button" id="add" class="btn btn-success pull-right add-dyn-input">
                                        <i class="fa fa-plus"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                            <div class="card">
                                <div id="add-general"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Imágen:</label>
                                </div>
                                <div class="col-sm-12">
                                    <div class="box js" title="Cargar Imágen">
                                        <input type="file" name="imagen" id="file-1" class="inputfile inputfile-6" accept="image/*" onchange="readURL(this, 'image-add');"/>
                                        <label for="file-1"><span id="span-add"></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Seleccione Imágen&hellip;</strong></label>
                                        <div class="col-sm-12">
                                            <img id="preview-image-add" class="sizing transparent" src="#" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-success" >Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-categoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class="fa fa-warning"></i>Editar Categoría</h4>
                </div>
                <form id="form-categoria-edit" class="form-edit" method="POST">
                    {{csrf_field()}}
                    {{ method_field('PATCH') }}
                    <div class="modal-body">
                        <div class="form-group" id="div-categoria-edit">
                            <label>Categoría:</label>
                            <input type="text" class="form-control boxed" name="categoria" id="categoria-edit">
                            <input type="hidden" name="categoria_id" id="categoria-id-edit" value="">
                        </div>
                        <div class="form-group" id="div-tipo-edit">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tipos:</label>
                                </div>
                                <div class="col-md-6" id="buttons-edit">
                                    <button type="button" id="edit" class="btn btn-success pull-right add-dyn-input">
                                        <i class="fa fa-plus"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                            <div class="card">
                                <div id="edit-general" class="tipo"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Imágen:</label>
                                </div>
                                <div class="col-sm-12">
                                    <div class="box js" title="Cargar Imágen">
                                        <input type="file" name="imagen" id="file-2" class="inputfile inputfile-6" accept="image/*" onchange="readURL(this, 'image-edit');"/>
                                        <label for="file-2"><span id="span-edit"></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Seleccione Imágen&hellip;</strong></label>
                                        <div class="col-sm-12">
                                            <img id="preview-image-edit" class="sizing" src="#" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-success" >Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-categoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-danger"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form id="form-categoria-del">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="categoria-del"></p>
                        <input type="hidden" name="categoria_id" id="cat-id" value="">
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" class="btn btn-primary" id="action" value="Si"></input>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- FIN MODAL CATEGORIAS -->

    <!-- MODAL MARCA -->
    <div class="modal fade" id="add-marca-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class="fa fa-warning"></i>Nueva Marca</h4>
                </div>
                <form id="form-marca-add">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group" id="div-marca">
                            <label>Marca:</label>
                            <input type="text" class="form-control boxed" name="marca" id="marca-add" placeholder="Ingrese Marca">
                        </div>
                        <div class="form-group" id="div-modelo">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Modelos:</label>
                                </div>
                                <div class="col-md-6" id="buttons-add">
                                    <button type="button" id="add" class="btn btn-success pull-right add-dyn-input">
                                        <i class="fa fa-plus"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                            <div class="card">
                                <div id="add-general"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-success" >Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-marca-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class="fa fa-warning"></i>Editar Marca</h4>
                </div>
                <form id="form-marca-edit" class="form-edit" method="POST">
                    {{csrf_field()}}
                    {{ method_field('PATCH') }}
                    <div class="modal-body">
                        <div class="form-group" id="div-marca-edit">
                            <label>Marca:</label>
                            <input type="text" class="form-control boxed" name="marca" id="marca-edit">
                            <input type="hidden" name="marca_id" id="marca-id-edit" value="">
                        </div>
                        <div class="form-group" id="div-modelo-edit">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Modelos:</label>
                                </div>
                                <div class="col-md-6" id="buttons-edit">
                                    <button type="button" id="edit" class="btn btn-success pull-right add-dyn-input">
                                        <i class="fa fa-plus"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                            <div class="card">
                                <div id="edit-general" class="modelo"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-success" >Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-marca-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-danger"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form id="form-marca-del">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="marca-del"></p>
                        <input type="hidden" name="marca_id" id="marca-id" value="">
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" class="btn btn-success" id="action" value="Si"></input>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- FIN MODAL MARCA -->
@endsection