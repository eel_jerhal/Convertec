@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/DataTables/datatables.css')}}"/>
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section id="productos">
        <div class="title-block">
            <h1 class="title"> Productos </h1>
            <p class="title-description"> Lista de productos en el sistema </p>
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <p id="output-success-producto"></p>
                            <table class="table table-bordered table-hover" id="productos-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Imágen</th>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Categoría</th>
                                        <th>Tipo</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section id="categorias">
        <div class="row">
            <div class="col-md-12 center-block">
                <div class="title-block title-margin">
                    <div class="row justify-content-between">
                        <div class="col-md-6">
                            <h1 class="title">Lista de Categorías</h1>
                            <p class="title-description">y tipos respectivos</p>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="open_form_categorias" class="btn btn-success pull-right" data-toggle="modal" data-target="#add-categoria-modal" title="Agregar Nueva Categoría">
                                <i class="fa fa-plus"></i>
                                Agregar Nueva Categoría
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 center-block">
                <div class="card">
                    <div class="card-block">
                        <p id="output-success-categoria"></p>
                        <table class="table table-bordered table-hover" id="categorias-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Imágen</th>
                                    <th>Categoría</th>
                                    <th>Tipos</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="marcas">
        <div class="row">
            <div class="col-md-12 center-block">
                <div class="title-block title-margin">
                    <div class="row justify-content-between">
                        <div class="col-md-6">
                            <h1 class="title">Lista de Marcas</h1>
                            <p class="title-description">y modelos respectivos</p>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="open_form_marcas" class="btn btn-success pull-right" data-toggle="modal" data-target="#add-marca-modal" title="Agregar Nueva Marca">
                                <i class="fa fa-plus"></i>
                                Agregar Nueva Marca
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 center-block">
                <div class="card">
                    <div class="card-block">
                        <p id="output-success-marca"></p>
                        <table class="table table-bordered table-hover" id="marcas-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Marca</th>
                                    <th>Modelos</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script> 
        var route = {
            //ruta productos
            'get_productos': '{{ route('get_data_productos') }}',
            'download_pdf': '{{ route('get_link_pdf') }}',
            'del_producto': '{{ route('del_data_producto') }}',
            //rutas categorias
            'get_categorias': '{{ route('get_data_categorias') }}',
            'set_categoria': '{{ route('set_data_categoria') }}',
            'upd_categoria': '{{ route('upd_data_categoria') }}',
            'del_categoria': '{{ route('del_data_categoria') }}',
            //rutas marcas
            'get_marcas': '{{ route('get_data_marcas') }}',
            'set_marca': '{{ route('set_data_marca') }}',
            'upd_marca': '{{ route('upd_data_marca') }}',
            'del_marca': '{{ route('del_data_marca') }}',
        };
    </script>
    <script type="text/javascript" src="{{asset('plugins/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset ('js/modal.js')}}"></script>
    <script src="{{asset ('js/tables.js')}}"></script>
    <script src="{{asset ('js/Productos/jquery.custom-file-input.js')}}"></script>
@endsection

@include('admin.modals')
