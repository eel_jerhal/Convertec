@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <div class="title-block">
        <h3 class="title"> Editar Producto {{$producto->codigo}}<span class="sparkline bar" data-type="bar"></span> </h3>
    </div>
    <form id="edit-producto" name="producto" enctype="multipart/form-data" action="{{route('productos.update', 'editar')}}" method="post">
        {{method_field('patch')}}
        {{csrf_field()}}
        <input type="hidden" name="producto_id" id="producto_id" value="{{$producto->id}}">
        <div class="card card-block">
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Categoría:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="categoria" id="categoria" title="Selección de Categoría">
                        <option value="">--Seleccione Categoría--</option>
                        @foreach ($categorias as $categoria)
                            <option value="{{ $categoria->id }}"> {{ $categoria->categoria }}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-sm-1 form-control-label text-xs-right">
                    Tipo:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="tipo" id="tipo" title="Selección de Tipo">
                        <option>--Seleccione Tipo--</option>
                    </select>
                </div>
                <div class="col-md-2"><span id="loader-tipos" class="d-none"><i class="fa fa-spinner fa-2x fa-spin"></i></span></div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Marca:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="marca" id="marca" title="Selección de Marca">
                        <option value="">--Seleccione Marca--</option>
                        @foreach ($marcas as $marca)
                            <option value="{{ $marca->id }}"> {{ $marca->marca }}</option>   
                        @endforeach
                    </select>
                </div>
                <label class="col-sm-1 form-control-label text-xs-right">
                    Modelo:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="modelo" id="modelo" title="Selección de Modelo">
                        <option>--Seleccione Modelo--</option>
                    </select>
                </div>
                <div class="col-md-2"><span id="loader-modelos" class="d-none"><i class="fa fa-spinner fa-2x fa-spin"></i></span></div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Nombre:
                </label>
                <div class="col-sm-3"> <input type="text" class="form-control boxed" name="nombre" id="nombre" value="{{ $producto->nombre }}" placeholder="Ingrese Nombre Producto"> </div>
                <label class="col-sm-1 form-control-label text-xs-right">
                    Código:
                </label>
                <div class="col-sm-3"> <input type="text" class="form-control boxed" name="codigo" id="codigo" value="{{ $producto->codigo }}" placeholder="Ingrese Código Producto"> </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Ficha Técnica:
                </label>
                <div class="col-sm-7">
                    <div class="box js" title="Cargar PDF">
                        <input type="file" name="pdf" id="file-2" class="inputfile inputfile-6" accept="application/pdf" onchange="readURL(this, 'pdf');"/>
                        <label for="file-2"><span>{{$pdfName}}</span>
                            <strong>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                </svg>
                                Seleccione PDF&hellip;
                            </strong>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-block">
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Descripción:
                </label>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <textarea id="descripcion" name="descripcion">{{ $producto->descripcion }}</textarea>
                </div>
            </div>
        </div>
        <div class="card card-block">
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Imágen:
                </label>
                <div class="col-sm-11">
                    <div class="box js" title="Cargar Imágen">
                        <input type="file" name="imagen" id="file-1" class="inputfile inputfile-6" accept="image/*" onchange="readURL(this, 'image');"/>
                    <label for="file-1"><span>{{$imageName}}</span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Seleccione Imágen&hellip;</strong></label>
                        <div class="col-lg-8">
                            <div class="card center-block">
                                <img id="preview-image" class="sizing" src="#" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12"> <button type="submit" class="btn btn-primary" id="btn_guardar" title="Guardar Producto">
                    Guardar
                    </button> 
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
        var error = {
                'error': '{{$errors->any()}}',
                'categoria': '{{old('categoria')}}',
                'tipo': '{{old('tipo')}}',
                'marca': '{{old('marca')}}',
                'modelo': '{{old('modelo')}}',
            };

        var valorSeleccionado = {
                'categoria': '{{$producto->categoria_id}}',
                'tipo': '{{$producto->tipo_id}}',
                'marca': '{{$producto->marca_id}}',
                'modelo': '{{$producto->modelo_id}}',
                'image': '{{asset("$image")}}',
            };
    </script>
    <script src="{{asset ('vendor/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset ('js/Productos/jquery.custom-file-input.js')}}"></script>
    <script src="{{asset ('js/Productos/textEditor-cfg.js')}}"></script>
    <script src="{{asset ('js/Productos/selectBoxLoad.js')}}"></script>
@endsection