<?php

use Illuminate\Database\Seeder;

class ModelosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modelos')->insert([
            [
                'modelo' => "14F08-E",
                'marca_id' => 1,
            ],
            [
                'modelo' => "HHC68A-2Z",
                'marca_id' => 1,
            ],
            [
                'modelo' => "PTF08A",
                'marca_id' => 1,
            ],
            [
                'modelo' => "FL-21",
                'marca_id' => 2,
            ],
            [
                'modelo' => "GLC1-D8011",
                'marca_id' => 2,
            ],
            [
                'modelo' => "GLP1-D4011",
                'marca_id' => 2,
            ],
            [
                'modelo' => "GR-96",
                'marca_id' => 2,
            ],
            [
                'modelo' => "LC1-D1210",
                'marca_id' => 2,
            ],
            [
                'modelo' => "LC1-D4011",
                'marca_id' => 2,
            ],
            [
                'modelo' => "SE-DP3",
                'marca_id' => 2,
            ],
            [
                'modelo' => "SE96U-9K4",
                'marca_id' => 2,   
            ],
            [
                'modelo' => "C1D-SA",
                'marca_id' => 3,
            ],
            [
                'modelo' => "MPJ2-S-248-C",
                'marca_id' => 4,
            ],
            [
                'modelo' => "NRP13-C12DH",
                'marca_id' => 5,   
            ]
        ]);
    }
}
