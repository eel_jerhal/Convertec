<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert([
            [
                'tipo' => "Bobina Móvil",
                'categoria_id' => 1,
            ],
            [
                'tipo' => "Contactor",
                'categoria_id' => 2,
            ],
            [
                'tipo' => "Digital",
                'categoria_id' => 1,
            ],
            [
                'tipo' => "DPDT",
                'categoria_id' => 2,
            ],
            [
                'tipo' => "Montaje a Riel Din",
                'categoria_id' => 2,
            ],
            [
                'tipo' => "Shunt",
                'categoria_id' => 1,
            ],
            [
                'tipo' => "Batería Abierta",
                'categoria_id' => 3,
            ],
            [
                'tipo' => "Batería Sellada",
                'categoria_id' => 3,
            ],
            [
                'tipo' => "Puesta en Marcha",
                'categoria_id' => 4,
            ],
            [
                'tipo' => "Mantención Banco de Baterías",
                'categoria_id' => 4,
            ]
        ]);
    }
}
