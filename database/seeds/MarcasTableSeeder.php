<?php

use Illuminate\Database\Seeder;

class MarcasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')->insert([
            [
                'marca' => "Clion",
            ],
            [
                'marca' => "Greegoo",
            ],
            [
                'marca' => "Klemsan",
            ],
            [
                'marca' => "Meishuo",
            ],
            [
                'marca' => "NCR",
            ]
        ]);
    }
}
