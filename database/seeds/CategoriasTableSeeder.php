<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            [
                'categoria' => "Instrumentos de Panel",
                'imagen' => "img/portafolio/fullsize/1.jpg",
            ],
            [
                'categoria' => "Equípos",
                'imagen' => "img/portafolio/fullsize/2.jpg",
            ],
            [
                'categoria' => "Baterías",
                'imagen' => "img/portafolio/fullsize/3.jpg",
            ],
            [
                'categoria' => "Servicios y Mantenciones",
                'imagen' => "img/portafolio/fullsize/4.jpg",
            ]
        ]);
    }
}
