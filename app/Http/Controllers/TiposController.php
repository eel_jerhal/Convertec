<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use \App\Tipo;

class TiposController extends CategoriasController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function destroy(Request $request)
    {
        $tipo = Tipo::findOrFail($request->tipo_id);
        if($tipo->delete()){
            $request->session()->flash('alert-warning', 'Tipo Eliminado con Exito!');
            return back();
        }
    }

    //Datatables
    public function get_data(){
        $tipo = Tipo::select('id', 'tipo', 'categoria');
        return DataTables($tipo)->make(true);
    }

}
