<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use \App\Producto;
use \App\Categoria;
use \App\Tipo;
use \App\Marca;
use \App\Modelo;
use App\Http\Requests\ProductoRequest;
use Illuminate\Support\Facades\Storage;

class ProductosController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        return View('admin.addproductos')
        ->with('categorias', Categoria::All())
        ->with('marcas', Marca::All());
    }

    public function store(ProductoRequest $request)
    {
        $image = $request->file('imagen'); //CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
        $pdf = $request->file('pdf');
        
        if($pdf != null){
            $pathPdf = $pdf->storeAs(
                'pdf', $pdf->getClientOriginalName(), 'public'
            );
        }else{
            $pathPdf = "";
        }

        if($image != null){
            $pathImg = $image->storeAs(
                'img/productos', $image->getClientOriginalName(), 'public'
            );
        }else{
            $pathImg = "img/logos/image-unavailable.png";
        }

        $prod = new Producto();
        $prod->nombre = $request->nombre;
        $prod->codigo = $request->codigo;
        $prod->categoria_id = $request->categoria;
        $prod->tipo_id = $request->tipo;
        $prod->marca_id = $request->marca;
        $prod->modelo_id = $request->modelo;
        $prod->descripcion = $request->descripcion;
        $prod->imagen = $pathImg;
        $prod->ficha = $pathPdf;

        if($prod->save()){
            $request->session()->flash('alert-success', 'Producto Ingresado con Exito!');
            return back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $producto = Producto::findOrFail($id);
        $image = $producto->imagen;
        $imageName = str_after($image, 'img/productos/');
        $pdf = $producto->ficha;
        $pdfName = str_after($pdf, 'pdf/');

        return View('admin.editproductos')
        ->with('producto', Producto::findOrFail($id))
        ->with('categorias', Categoria::All())
        ->with('tipos', Tipo::All())
        ->with('marcas', Marca::All())
        ->with('modelos', Modelo::All())
        ->with('pdf', $pdf)
        ->with('pdfName', $pdfName)
        ->with('image', $image)
        ->with('imageName', $imageName);
    }

    public function update(ProductoRequest $request)
    {
        $image = $request->file('imagen'); //CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
        $pdf = $request->file('pdf');
        $prod = Producto::findOrFail($request->producto_id);
        
        if($pdf != null){
            $pathPdf = str_after(
                $pdf->storeAs(
                    'public/pdf', $pdf->getClientOriginalName()
                ),
                'public/'
            );
            Storage::delete('public/'.$prod->ficha);
            $prod->ficha = $pathPdf;
        }

        if($image != null){
            $pathImg = str_after(
                $image->storeAs(
                    'public/img/productos', $image->getClientOriginalName()
                ),
                'public/'
            );
            Storage::delete('public/'.$prod->imagen);
            $prod->imagen = $pathImg;
        }

        $prod->codigo = $request->codigo;
        $prod->nombre = $request->nombre;
        $prod->categoria_id = $request->categoria;
        $prod->tipo_id = $request->tipo;
        $prod->marca_id = $request->marca;
        $prod->modelo_id = $request->modelo;
        $prod->descripcion = $request->descripcion;

        if($prod->save()){
        $request->session()->flash('alert-success', 'Producto Modificado con Exito!');
        return redirect()->route('admin');
    }
    }

    public function destroy(Request $request)
    {
        $prod = Producto::findOrFail($request->producto_id);
        Storage::delete('public/'.$prod->imagen);
        if($prod->ficha){
            Storage::delete('public/'.$prod->ficha);
        }
        if($prod->delete()){
            $mensaje = "<div class='alert alert-warning'>Producto Eliminado con Exito!</div>";
            echo json_encode($mensaje);
        }
    }

    //datatable
    public function get_data(){
        $producto = Producto::join('marcas', 'marca_id', '=', 'marcas.id')
            ->join('tipos', 'tipo_id', '=', 'tipos.id')
            ->join('modelos', 'modelo_id', '=', 'modelos.id')
            ->join('categorias', 'productos.categoria_id', '=', 'categorias.id')
            ->select(
                'productos.id', 'productos.codigo', 'productos.nombre', 
                'productos.descripcion', 'tipos.tipo', 'categorias.categoria', 
                'marcas.marca', 'modelos.modelo', 'productos.imagen', 'productos.ficha'
            )
        ->get();
        
        return DataTables($producto)->make(true);
    }

}
