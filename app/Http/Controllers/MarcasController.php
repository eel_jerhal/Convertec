<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use \App\Marca;
use \App\Modelo;
use App\Http\Requests\MarcaRequest;

class MarcasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(MarcaRequest $request)
    {   
        $marca = new Marca();
        $marca->marca = $request->marca;
        $marca->save();
        foreach($request->dynamicInput as $modelo_v){
            $modelo = new Modelo();
            $modelo->modelo = $modelo_v;
            $modelo->marca_id = $marca->id;
            $modelo->save();
        }
        
        $success_output = '<div class="alert alert-success">Marca Ingresada con Exito!</div>';
        echo json_encode($success_output);
    }

    public function update(MarcaRequest $request)
    {
        $cantModelosModificada = count($request->dynamicInput_id);
        $cantModelosTotales = count($request->dynamicInput);
        $marca = Marca::findOrFail($request->marca_id);
        $success_output = ['success' =>''];

        //ELIMINAR MODELOS
        foreach ($request->delete as $deleteModelo) {
            if ($deleteModelo != "") {
                $modelo = Modelo::findOrFail($deleteModelo);
                $modelo->delete();
            }
        }

        //ACTUALIZAR MODELOS
        $this->updateModelo($marca, $cantModelosModificada, $request);

        //AGREGAR MODELOS
        if ($cantModelosTotales > $cantModelosModificada) {
            for($i = $cantModelosModificada; $i < $cantModelosTotales ; $i++){
                if($request->dynamicInput[$i] != null){
                    $modelo = new Modelo();
                    $modelo->modelo = $request->dynamicInput[$i];
                    $modelo->marca_id = $marca->id;
                    $modelo->save();
                }
            }
        }
        
        $success_output['success'] = '<div class="alert alert-success">Marca Modificada con Exito!</div>';
        echo json_encode($success_output);

    }

    private function updateModelo(Marca $marca, Int $cantModelosModificada, MarcaRequest $request)
    {
        $marca->marca = $request->marca;
        $marca->save();
        for($i = 0; $i < $cantModelosModificada; $i++){
            $modelo = Modelo::findOrFail($request->dynamicInput_id[$i]);
            $modelo->modelo = $request->dynamicInput[$i];
            $modelo->save();
        }
    }

    public function destroy(Request $request)
    {
        $marca = Marca::findOrFail($request->marca_id);
        if($marca->delete()){
            $mensaje = '<div class="alert alert-warning">Marca Eliminada con Exito!</div>';
            echo json_encode($mensaje);
        }
    }

    public function destroyModelo(Request $request)
    {
        $modelos = Modelo::where('marca_id',$request->marca_id)->whereNotIn('id', $request->dynamicInput_id)->get();
        foreach ($modelos as $i => $modelo) {
            $modelo->delete();
        }
    }

    //Datatables
    public function get_data()
    {
        $marca = Marca::with('modelos')->get();
        return DataTables($marca)->make(true);       
    }

    public function getModelos($id) {
        $modelos = Modelo::where("marca_id",$id)->pluck("modelo","id");
        return json_encode($modelos);
    }
}
