<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use Image;
use \App\Categoria;
use \App\Tipo;
use App\Http\Requests\CategoriaRequest;
use Illuminate\Http\UploadedFile;

class CategoriasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(CategoriaRequest $request)
    {   
        $image = $request->file('imagen'); //CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
        if($image == null){
            $pathImg = "img/logos/image-unavailable_650-350.png";
        }else{
            $pathImg = $this->uploadFile($image);
        }

        $categoria = new Categoria();
        $categoria->categoria = $request->categoria;
        $categoria->imagen = $pathImg;
        $categoria->save();
        foreach($request->dynamicInput as $tipo_v){
            $tipo = new Tipo();
            $tipo->tipo = $tipo_v;
            $tipo->categoria_id = $categoria->id;
            $tipo->save();
        }
        
        $success_output = '<div class="alert alert-success">Categoría Ingresada con Exito!</div>';
        echo json_encode($success_output);
    }

    public function update(CategoriaRequest $request)
    {
        $cantTiposModificada = count($request->dynamicInput_id);
        $cantTiposTotales = count($request->dynamicInput);
        $categoria = Categoria::findOrFail($request->categoria_id);
        $success_output = ['success' =>''];

        //ELIMINAR TIPOS
        foreach ($request->delete as $deleteTipo) {
            if ($deleteTipo != "") {
                $tipo = Tipo::findOrFail($deleteTipo);
                $tipo->delete();
            }
        }

        //ACTUALIZAR TIPOS
        $this->updateTipo($categoria, $cantTiposModificada, $request);

        //AGREGAR TIPOS
        if ($cantTiposTotales > $cantTiposModificada) {
            for($i = $cantTiposModificada; $i < $cantTiposTotales ; $i++){
                if($request->dynamicInput[$i] != null){
                    $tipo = new Tipo();
                    $tipo->tipo = $request->dynamicInput[$i];
                    $tipo->categoria_id = $categoria->id;
                    $tipo->save();
                }
            }
        }
        
        $success_output['success'] = '<div class="alert alert-success">Categoría Modificada con Exito!</div>';
        echo json_encode($success_output);

    }

    private function updateTipo(Categoria $categoria, Int $cantTiposModificada, CategoriaRequest $request)
    {
        $image = $request->file('imagen'); //CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
        if($image != null){
            $pathImg = $this->uploadFile($image);
            Storage::delete('public/'.$categoria->imagen);
            $categoria->imagen = $pathImg;
        }

        $categoria->categoria = $request->categoria;
        $categoria->save();
        for($i = 0; $i < $cantTiposModificada; $i++){
            $tipo = Tipo::findOrFail($request->dynamicInput_id[$i]);
            $tipo->tipo = $request->dynamicInput[$i];
            $tipo->save();
        }
    }

    public function destroy(Request $request)
    {
        $categoria = Categoria::findOrFail($request->categoria_id);
        Storage::delete('public/'.$categoria->imagen);
        if($categoria->delete()){
            $mensaje = '<div class="alert alert-warning">Categoría Eliminada con Exito!</div>';
            echo json_encode($mensaje);
        }
    }

    public function destroyTipo(Request $request)
    {
        $tipos = Tipo::where('categoria_id',$request->categoria_id)->whereNotIn('id', $request->dynamicInput_id)->get();
        foreach ($tipos as $i => $tipo) {
            $tipo->delete();
        }
    }

    //Datatables
    public function get_data()
    {
        $categoria = Categoria::with('tipos')->get();
        return DataTables($categoria)->make(true);       
    }

    public function getTipos($id) {
        $tipos = Tipo::where("categoria_id",$id)->pluck("tipo","id");
        return json_encode($tipos);
    }

    private function uploadFile(UploadedFile $image){
        $pathImg = $image->storeAs(
            'img/categorias', $image->getClientOriginalName(), 'public'
        );
        $imagen = Image::make(Storage::get('public/'.$pathImg))->resize(650,350)->encode();
        Storage::put('public/'.$pathImg, $imagen);
        return $pathImg;
    }

}
