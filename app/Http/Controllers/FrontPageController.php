<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Categoria;
use \App\Tipo;
use \App\Producto;
use \App\ImgProducto;

class FrontPageController extends Controller
{
    public function index()
    {
        return View('front.welcome')
        ->with('categorias', Categoria::All());
    }

    public function showCategoria($id)
    {
        $categorias = Categoria::with('tipos')->get();
        return View('front.categoria')
        ->with('categorias', $categorias)
        ->with('productos', Producto::where('categoria_id', $id)->paginate(3));
    }
}
