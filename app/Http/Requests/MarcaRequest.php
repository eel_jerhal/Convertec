<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarcaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'marca' => 'required|min:2|max:255|unique:marcas,marca',
                    'dynamicInput.*' => 'required|min:2|max:255|distinct|unique:modelos,modelo',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                $marcaId = $this->input('marca_id');
                $modeloId = $this->input('dynamicInput_id');

                $rules = [
                    'marca' => "required|min:2|max:255|unique:marcas,marca,$marcaId",
                  ];
                
                  foreach($modeloId as $key => $val)
                  {
                    $rules['dynamicInput.'.$key] = "required|min:2|max:255|distinct|unique:modelos,modelo,$modeloId[$key]";
                  }
                
                  return $rules;
            }
            default:break;
        }
    }

   /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'marca.required' => 'El campo Marca es obligatorio.',
            'marca.unique' => 'El campo Marca se encuentra duplicado en la Base de Datos.',
        ];
        
        foreach($this->request->get('dynamicInput') as $key => $val)
        {
            $messages["dynamicInput.$key.required"] = "El Modelo: " .($key + 1)." está sin rellenar.";
            $messages["dynamicInput.$key.unique"] = "El Modelo: " .($key + 1)." ya se encuentra ingresado.";
            $messages["dynamicInput.$key.distinct"] = "El Modelo: " .($key + 1)." ya está en el formulario.";
            $messages["dynamicInput.$key.min"] = "El Modelo: " .($key + 1)." debe tener más de :min caracteres.";
            
        }

        return $messages;
    }
}
