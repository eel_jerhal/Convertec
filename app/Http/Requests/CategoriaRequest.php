<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'categoria' => 'required|min:2|max:255|unique:categorias,categoria',
                    'dynamicInput.*' => 'required|min:2|max:255|distinct|unique:tipos,tipo',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                $categoriaId = $this->input('categoria_id');
                $tipoId = $this->input('dynamicInput_id');

                $rules = [
                    'categoria' => "required|min:2|max:255|unique:categorias,categoria,$categoriaId",
                  ];
                
                  foreach($tipoId as $key => $val)
                  {
                    $rules['dynamicInput.'.$key] = "required|min:2|max:255|distinct|unique:tipos,tipo,$tipoId[$key]";
                  }
                
                  return $rules;
            }
            default:break;
        }
    }

   /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'categoria.required' => 'El campo Categoría es obligatorio.',
            'categoria.unique' => 'El campo Categoría se encuentra duplicado en la Base de Datos.',
        ];
        
        foreach($this->request->get('dynamicInput') as $key => $val)
        {
            $messages["dynamicInput.$key.required"] = "El Tipo: " .($key + 1)." está sin rellenar.";
            $messages["dynamicInput.$key.unique"] = "El Tipo: " .($key + 1)." ya se encuentra ingresado.";
            $messages["dynamicInput.$key.distinct"] = "El Tipo: " .($key + 1)." ya está en el formulario.";
            $messages["dynamicInput.$key.min"] = "El Tipo: " .($key + 1)." debe tener más de :min caracteres.";
            
        }

        return $messages;
    }
}
