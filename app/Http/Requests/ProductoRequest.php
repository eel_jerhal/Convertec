<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'codigo' => 'required|min:2|max:255|unique:productos,codigo',
                    'nombre' => 'required|min:2|max:255|',
                    'categoria' => 'required',
                    'marca' => 'required',
                    'modelo' => 'required',
                    'tipo' => 'required',
                    'descripcion' => 'required|min:2|max:255',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                $productoId = $this->input('producto_id');

                $rules = [
                    'codigo' => "required|min:2|max:255|unique:productos,codigo,$productoId",
                    'categoria' => 'required',
                    'marca' => 'required',
                    'modelo' => 'required',
                    'tipo' => 'required',
                    'descripcion' => 'required|min:2|max:255',
                ];

                return $rules;
            }
            default:break;
        }
    }

   /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'codigo.required' => 'El campo Código es obligatorio.',
            'nombre.required' => 'El campo Nombre es obligatorio.',
            'codigo.unique' => 'El campo Código se encuentra duplicado en la Base de Datos.',
            'categoria.required' => 'El campo Categoría es obligatorio.',
            'marca.required' => 'El campo Marca es obligatorio.',
            'modelo.required' => 'El campo Modelo es obligatorio.',
            'tipo.required' => 'El campo Tipo es obligatorio.',
            'descripcion.required' => 'El campo Descripción es obligatorio.',
        ];
    }
}
