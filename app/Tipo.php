<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    /* use SoftDeletes; */

    protected $table = "tipos";

    protected $fillable = [
        'tipo', 'categoria_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'categoria_id',
    ];

    public function productos()
    {
    	return $this->hasMany('\App\Producto');
    }

    public function categoria()
    {
        return $this->belongsTo('\App\Categoria');
    }
}
