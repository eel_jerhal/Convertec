<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = "marcas";

    protected $fillable = [
        'marca',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function productos()
    {
    	return $this->hasMany('\App\Producto');
    }

    public function modelos(){
        return $this->HasMany('\App\Modelo');
    }
}
