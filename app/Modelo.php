<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $table = "modelos";

    protected $fillable = [
        'modelo', 'modelo_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'marca_id',
    ];

    public function productos()
    {
    	return $this->hasMany('\App\Producto');
    }

    public function marca()
    {
        return $this->belongsTo('\App\Marca');
    }
}
