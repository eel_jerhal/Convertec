<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "productos";


    protected $fillable = [
        'codigo', 'nombre', 'tipo_id', 'marca_id', 'categoria_id', 'descripcion', 'imagen', 'ficha',
    ];

    public function categoria()
    {
    	return $this->belongsTo('Categoria');
    }

    public function marca()
    {
    	return $this->belongsTo('Marca');
    }

    public function tipo()
    {
    	return $this->belongsTo('Tipo');
    }

    public function imgproductos(){
        return $this->HasMany('Imgproducto');
    }
}
