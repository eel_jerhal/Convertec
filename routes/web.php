<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontPageController@index')->name('welcome');

Route::get('/admin/instructivo', function () {
    return view('admin.instructivo');
});

Route::get('/categoria/{id}', 'FrontPageController@showCategoria')->name('categoria');

Auth::routes();

//Route::get('/admin', 'PrincipalController@index');
Route::get('/admin', ['as' => 'admin', 'uses' => 'PrincipalController@index']);
Route::get('/admin/productos/get_data_productos', ['as' => 'get_data_productos', 'uses' => 'ProductosController@get_data']);
Route::get('/admin/productos/get_link_pdf', ['as' => 'get_link_pdf', 'uses' => 'ProductosController@download_pdf']);
Route::get('/admin/categorias/get_data_categorias', ['as' => 'get_data_categorias', 'uses' => 'CategoriasController@get_data']);
Route::get('/admin/marcas/get_data_marcas', ['as' => 'get_data_marcas', 'uses' =>'MarcasController@get_data']);
//CATEGORIAS
Route::get('/admin/productos/get_tipos/{id}', 'CategoriasController@getTipos')->name('get_tipos_selectbox');
Route::post('/admin/categorias/set_data_categoria', 'CategoriasController@store')->name('set_data_categoria');
Route::patch('/admin/categorias/upd_data_categoria', 'CategoriasController@update')->name('upd_data_categoria');
Route::delete('/admin/categorias/del_data_categoria', 'CategoriasController@destroy')->name('del_data_categoria');
//MARCAS
Route::get('/admin/productos/get_modelos/{id}', 'MarcasController@getModelos')->name('get_modelos_selectbox');
Route::post('/admin/marcas/set_data_marca', 'MarcasController@store')->name('set_data_marca');
Route::patch('/admin/marcas/upd_data_marca', 'MarcasController@update')->name('upd_data_marca');
Route::delete('/admin/marcas/del_data_marca', 'MarcasController@destroy')->name('del_data_marca');
//PRODUCTOS
Route::resource('/admin/productos', 'ProductosController');
Route::post('/admin/productos/set_data_producto', 'ProductosController@store')->name('set_data_producto');
Route::delete('/admin/productos/del_data_producto', 'ProductosController@destroy')->name('del_data_producto');
/* Route::resource('/admin/categorias', 'CategoriasController'); */
/* Route::resource('/admin/marcas', 'MarcasController'); */