Página de administración de productos para la empresa Convertec utilizando framework de PHP, Laravel.

## Instalación (para desarrollo)

- Instalar xampp y composer.
- Vincular el repo (luego git fetch)
- Descargar e Instalar valet (Windows) ```$ composer global require cretueusebiu/valet-windows``` instalar en cmd como administrador ```$ valet install```.
- Linkear en valet la carpeta de desarrollo valet link, valet park
- Linkear phpmyadmin a valet (para utilizar phpmyadmin.test)
- Instalar laravel por composer ```$ composer global require laravel/installer```
- Crear env usando el env.example ```$ cp .env.example .env ``` generar una key ```$ php artisan key:generate``` y modificar archivo, vincular BD, etc.
- En caso de problemas limpiar config cache y guardar la nueva configuración  ```$ php artisan config: clear``` y ```$ php artisan config:cache```
- Crear la BD con el nombre vinculado en .env y ejecutar la creación de las tablas ```$ php artisan migrate:refresh --seed```
- Updatear composer ```$ composer update --no-scripts```.
- Configurar Acrylic DNS ipv4 e ipv6 con 127.0.0.1 y ::1 respectivamente.
- Linkear storage con ```$ php artisan storage:link```
- EZ

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Pulse Storm](http://www.pulsestorm.net/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
